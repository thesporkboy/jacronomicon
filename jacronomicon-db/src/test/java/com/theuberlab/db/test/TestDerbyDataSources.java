package com.theuberlab.db.test;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.LoggerFactory;

import com.theuberlab.db.derby.DerbyDataSources;
import com.theuberlab.jacronomicon.core.Acronym;

/**
 * TestDerbyDataSources 
 * 
 * @author Aaron Forster 
 * @date Feb 3, 2015
 */
public class TestDerbyDataSources {
	/*
	 * ###########################################################
	 *    Attributes
	 * ###########################################################
	 */
	private final static org.slf4j.Logger logger = LoggerFactory
			.getLogger(TestDerbyDataSources.class);
	private DerbyDataSources dataSource = null;
	
	/**
	 * 
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		dataSource = new DerbyDataSources();
	}

	/**
	 * 
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.theuberlab.db.derby.DerbyDataSources#getCategoryNames()}.
	 */
	@Test
	public void testGetCategoryNames() {
		List<String> categoryNames = dataSource.getCategoryNames();
		
		assertTrue(categoryNames.contains("General"));
	}
	
	@Test
	public void testGetAcronymsByCategoryName() {
		List<Acronym> itAcronyms = dataSource.getAcronymsByCategoryName("Information Technology (IT)");
		List<Acronym> genAcronyms = dataSource.getAcronymsByCategoryName("General");
		
		logger.debug("List element 0 has [{}]",itAcronyms.get(0).getShortName());
		assertTrue(itAcronyms.get(0).getShortName().equals("AJAX"));
	}
	
	@Test
	public void testGetByAcronymShorNameContents() {
		List<Acronym> itAcronyms = dataSource.getByAcronymShorNameContents("CM");
		
		logger.debug("List element 0 has [{}]",itAcronyms.get(0).getShortName());
		assertTrue(itAcronyms.get(0).getShortName().equals("CMM"));
	}

	/*
	 * ###########################################################
	 *    Constructors
	 * ###########################################################
	 */

	/*
	 * ###########################################################
	 *    Methods
	 * ###########################################################
	 */

	/*
	 * ###########################################################
	 *    Getters and Setters
	 * ###########################################################
	 */
}
