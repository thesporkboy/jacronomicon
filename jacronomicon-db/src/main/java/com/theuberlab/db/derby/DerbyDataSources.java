package com.theuberlab.db.derby;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.LoggerFactory;

import com.theuberlab.jacronomicon.core.Acronym;

/**
 * DerbyDataSources 
 * 
 * @author Aaron Forster 
 * @date Feb 3, 2015
 */
public class DerbyDataSources {
	/*
	 * ###########################################################
	 *    Attributes
	 * ###########################################################
	 */
	private final static org.slf4j.Logger logger = LoggerFactory
			.getLogger(DerbyDataSources.class);
	protected static String databaseURL = "jdbc:derby:classpath:builtInAcronyms";
	// private static String databaseURL =
	// "jdbc:derby://localhost:1527/myDB;create=true;user=me;password=mine";
//	protected static String tableName = "restaurants";
	// jdbc Connection
	/** A container for the database connection. */
	protected static Connection connection = null;
	/** A container for the compiled statement. */
	protected static Statement statement = null;
	/** The driver to use to connect to the database. */
	protected static String driverClass = "org.apache.derby.jdbc.EmbeddedDriver";
	/** True for built in databases. Datasets accessed from within jars will always be read only. */
	protected boolean builtIn;
	/** True if the connection to the database has been established. */
	protected boolean initialized = false;
	/** The table which contains categories. */
	protected String categoryTable = "ACRONYM_CATEGORIES";
	/** The table which contains acronyms. */
	protected String acronymTable = "ACRONYMS";
	/** A SQL statement which will return all categories from the categories table. */
	protected String categoryListStatement = "select * from ";
	/** A SQL statement which will return all acronyms from the acronym table. */
	protected String acronymListStatement = "select * from ";
	
	
	/*
	 * ###########################################################
	 *    Constructors
	 * ###########################################################
	 */
	public DerbyDataSources() {
		init();
	}
	
	/*
	 * ###########################################################
	 *    Methods
	 * ###########################################################
	 */
	// Use this to connect to a file specified on the command line.
//		private static String databaseURL = "jdbc:derby:/Users/x4e5/Documents/Code/EclipseJavaWorkspaces/NordClipse/derbyexperiments/src/main/resources/testApacheDerbyDB;";
		// Use this to connect to a database which is included within the jar itself.
	
	/**
	 * Initialize the connection to the derby database.
	 */
	private static void init() {
		try {
			// Create an instance of the derby driver.
			Class.forName(driverClass).newInstance();
			// For non-embeded database use the ClientDriver and then specify a
			// server and port in the connection string.
			// Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
			// ClientDriver can also be used to connect to a file based db by
			// specifying //localhost/path/to/file
			
			// Connect to the database.
			connection = DriverManager.getConnection(databaseURL);
			
			// Set initialized to true
			
		} catch (InstantiationException e) {
			logger.error("Failed with InstantiationException: {}",
					e.getMessage());
			logger.debug("Full stack trace:", e);
		} catch (IllegalAccessException e) {
			logger.error("Failed with IllegalAccessException: {}",
					e.getMessage());
			logger.debug("Full stack trace:", e);
		} catch (ClassNotFoundException e) {
			logger.error("Failed with ClassNotFoundException: {}",
					e.getMessage());
			logger.debug("Full stack trace:", e);
		} catch (SQLException e) {
			logger.error("Failed with SQLException: {}", e.getMessage());
			logger.debug("Full stack trace:", e);
		}
		

	}
	
	private static void shutdown() {
		// Destroy the statement object if it has been initialized.
		try {
			if (statement != null) {
				statement.close();
			}
			// Check and see if the database is initialized.
			if (connection != null) {
				// If so shut down the database.
				DriverManager.getConnection(databaseURL + ";shutdown=true");
				connection.close();
			}
		} catch (SQLException e) {
			logger.error("Failed with Exception: {}", e.getMessage());
			logger.debug("Full stack trace:", e);
		}

	}
	
	/*
	 * ###########################################################
	 *    Getters and Setters
	 * ###########################################################
	 */
	
	/**
	 * Returns a list of all the categories found in this dataset.
	 * 
	 * @return
	 */
	public List<String> getCategoryNames() {
		List<String> results = new ArrayList<String>();
		
		try {
			statement = connection.createStatement();
			ResultSet searchResults = statement.executeQuery(categoryListStatement + categoryTable);
			ResultSetMetaData rsmd = searchResults.getMetaData();
			
			//TODO: Wrap this in a check to ensure we only do it if we're logging in debug.
			int numberCols = rsmd.getColumnCount();
			String columnNames = "";
			for (int i = 1; i <= numberCols; i++) {
				// print Column Names
				columnNames = columnNames + rsmd.getColumnLabel(i) + "\t\t";
			}
			logger.debug(columnNames);
			
			while (searchResults.next()) {
				int id = searchResults.getInt(1);
				// Get the category name.
				String category = searchResults.getString(2);
				// Add it to the list.
				results.add(category);
				
				String description = searchResults.getString(3);
				logger.debug(id + "\t\t" + category + "\t\t" + description);
			}
			
			searchResults.close();
			statement.close();
		} catch (SQLException e) {
			logger.error("Failed with Exception: {}", e.getMessage());
			logger.debug("Full stack trace:", e);
		}
		
		return results;
	}
	//TODO: Update to handle if there are no results.
	/**
	 * Gets all acronyms for the specified category.
	 * 
	 * @param categoryName
	 * @return
	 */
	public List<Acronym> getAcronymsByCategoryName(String categoryName) {
		logger.debug("Running getAcronymsByCategoryName({})",categoryName);
		List<Acronym> results = new ArrayList<Acronym>();
		
		// This is the proper statement.
//		select acr.shortname, acr.fullname, acr.description
//		from acronyms acr left outer join acronym_categories cat on acr.category = cat.category_id
//		where name='Information Technology (IT)';
		
		String selectStatement = "select acr.shortname, acr.fullname, acr.description"
				+ " from acronyms acr left outer join acronym_categories cat on acr.category = cat.category_id"
				+ " where name='" + categoryName + "'";
		
		logger.debug("Executing select statement [{}]",selectStatement);
		
		try {
			statement = connection.createStatement();
			ResultSet searchResults = statement.executeQuery(selectStatement);
			ResultSetMetaData rsmd = searchResults.getMetaData();
			
//			//TODO: Wrap this in a check to ensure we only do it if we're logging in debug.
//			int numberCols = rsmd.getColumnCount();
//			String columnNames = "";
//			for (int i = 1; i <= numberCols; i++) {
//				// print Column Names
//				columnNames = columnNames + rsmd.getColumnLabel(i) + "\t\t";
//			}
//			logger.debug(columnNames);
			
			while (searchResults.next()) {
				// Get the acronym.
				String shortName = searchResults.getString(1);
				String longName = searchResults.getString(2);
				String description = searchResults.getString(3);
				Acronym thisAronym = new Acronym(shortName,longName,description,categoryName);
				// Add it to the list.
				results.add(thisAronym);
				logger.debug("Adding result [{}] [{}] [{}] [{}] to the results list", shortName, longName, description, categoryName);
			}
			
			searchResults.close();
			statement.close();
		} catch (SQLException e) {
			logger.error("Failed with Exception: {}", e.getMessage());
			logger.debug("Full stack trace:", e);
		}
		

		
		return results;
	}
	
	/**
	 * Returns a list of acronyms which contain the supplied string.
	 * For example for the list of acronyms "TLD" "TLA" "MCC" "FTP"
	 * a search of "TL" will return "TLD" and "TLA."
	 * however a search of "LA" will return only "TLA"
	 * 
	 * @param search
	 * @return
	 */
	public List<Acronym> getByAcronymShorNameContents(String search) {
		logger.debug("Running getByAcronymShorNameContents({})",search);
		List<Acronym> results = new ArrayList<Acronym>();
		
		// This is the proper statement.
//		select acr.shortname, acr.fullname, acr.description
//		from acronyms acr left outer join acronym_categories cat on acr.category = cat.category_id
//		where name='Information Technology (IT)';
		
		String selectStatement = "select acr.shortname, acr.fullname, acr.description, cat.name"
				+ " from acronyms acr left outer join acronym_categories cat on acr.category = cat.category_id"
				+ " where shortname LIKE '%" + search + "%'";
		
		logger.debug("Executing select statement [{}]",selectStatement);
		
		try {
			statement = connection.createStatement();
			ResultSet searchResults = statement.executeQuery(selectStatement);
			ResultSetMetaData rsmd = searchResults.getMetaData();
			
//			//TODO: Wrap this in a check to ensure we only do it if we're logging in debug.
//			int numberCols = rsmd.getColumnCount();
//			String columnNames = "";
//			for (int i = 1; i <= numberCols; i++) {
//				// print Column Names
//				columnNames = columnNames + rsmd.getColumnLabel(i) + "\t\t";
//			}
//			logger.debug(columnNames);
			
			while (searchResults.next()) {
				// Get the acronym.
				String shortName = searchResults.getString(1);
				String longName = searchResults.getString(2);
				String description = searchResults.getString(3);
				String categoryName = searchResults.getString(4);
				
				Acronym thisAronym = new Acronym(shortName,longName,description,categoryName);
				// Add it to the list.
				results.add(thisAronym);
				logger.debug("Adding result [{}] [{}] [{}] [{}] to the results list", shortName, longName, description, categoryName);
			}
			
			searchResults.close();
			statement.close();
		} catch (SQLException e) {
			logger.error("Failed with Exception: {}", e.getMessage());
			logger.debug("Full stack trace:", e);
		}
		

		
		return results;
	}
}
