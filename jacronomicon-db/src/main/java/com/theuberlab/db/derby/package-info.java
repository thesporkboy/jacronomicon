/**
 * Apache Derby is used for accessing the built in datasource 
 * and for a user generated custom datasource managed through the application.
 * 
 * @author Aaron Forster 
 * @date Feb 3, 2015
 */
package com.theuberlab.db.derby;