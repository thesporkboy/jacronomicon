/**
 * jacronomicon will also support a local text based data source 
 * for users that have all thier stuff but don't want to add it through the GUI.
 * 
 * @author Aaron Forster 
 * @date Feb 3, 2015
 */
package com.theuberlab.db.text;