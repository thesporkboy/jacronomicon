/* Create the categories table. */
CREATE TABLE acronym_categories (
	category_id INT NOT NULL PRIMARY KEY GENERATED ALWAYS AS IDENTITY 
                (START WITH 1, INCREMENT BY 1),
	name VARCHAR(64) NOT NULL UNIQUE,
	description VARCHAR(512));

/* Create the acronyms table. */
CREATE TABLE acronyms (
	acronym_id INT NOT NULL PRIMARY KEY GENERATED ALWAYS AS IDENTITY 
                (START WITH 1, INCREMENT BY 1),
	shortname VARCHAR(12),
	fullname VARCHAR(48),
	description VARCHAR(512),
	category INT NOT NULL REFERENCES acronym_categories(category_id));

/* Populate categories with records. */
INSERT INTO acronym_categories(name,description)
VALUES ('General','Acronyms which do not belong to a specific technology, industry or group.');
INSERT INTO acronym_categories(name,description)
VALUES ('Information Technology (IT)','The technology business sector.');

/* Populate acronyms with records. */
INSERT INTO acronyms(shortname,fullname,description,category)
VALUES ('TLA','Three Letter Acronym','A jokingly self-describing acronym.', 1);
INSERT INTO acronyms(shortname,fullname,description,category)
VALUES ('AJAX','Asynchronous Javascript and XML','A technology, pioneered by Google to make web sites more interactive.', 2);
INSERT INTO acronyms(shortname,fullname,description,category)
VALUES ('CMM','Communication Media Module (Cisco)','', 2);
INSERT INTO acronyms(shortname,fullname,description,category)
VALUES ('CMM','Contextual Menu Module','', 2);
INSERT INTO acronyms(shortname,fullname,description,category)
VALUES ('CMM','Contextual Menu Manager (Apple Macintosh)','', 2);
INSERT INTO acronyms(shortname,fullname,description,category)
VALUES ('DHCP','Dynamic Host Configuration Protocol (protocol for automating the configuration of computers that use TCP/IP)','', 2);
INSERT INTO acronyms(shortname,fullname,description,category)
VALUES ('DHCP','Decentralized Hospital Computer Program','', 2);