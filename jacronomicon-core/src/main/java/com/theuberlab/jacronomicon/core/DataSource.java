package com.theuberlab.jacronomicon.core;

import org.slf4j.LoggerFactory;

/**
 * DataSource represents a data source used for storing acronyms on disk. 
 * 
 * @author Aaron Forster 
 * @date Feb 3, 2015
 */
public class DataSource {
	/*
	 * ###########################################################
	 *    Attributes
	 * ###########################################################
	 */
	private final static org.slf4j.Logger logger = LoggerFactory
			.getLogger(DataSource.class);
	/** The human readable name of the datasource. */
	protected String dataSourceName;
	/** The category of the datasource. */
	protected String dataSourceCategory;
	/** True if this is a user defined datasource. */
	protected boolean custom;
	/** The filesystem path (if any) to the datasource. */
	protected String dbFilePath;
	
	/*
	 * ###########################################################
	 *    Constructors
	 * ###########################################################
	 */

	/*
	 * ###########################################################
	 *    Methods
	 * ###########################################################
	 */

	/*
	 * ###########################################################
	 *    Getters and Setters
	 * ###########################################################
	 */
}