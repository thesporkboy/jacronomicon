package com.theuberlab.jacronomicon.core;

import org.slf4j.LoggerFactory;

/**
 * AcronymFinder is responsible for taking an an acronym or part of
 * an acronym, finding it across the appropriate datasources and
 * returning the Acronym object for that acronym.
 * 
 * @author Aaron Forster 
 * @date Feb 3, 2015
 */
public class AcronymFinder {
	/*
	 * ###########################################################
	 *    Attributes
	 * ###########################################################
	 */
	private final static org.slf4j.Logger logger = LoggerFactory
			.getLogger(AcronymFinder.class);

	/*
	 * ###########################################################
	 *    Constructors
	 * ###########################################################
	 */

	/*
	 * ###########################################################
	 *    Methods
	 * ###########################################################
	 */

	/*
	 * ###########################################################
	 *    Getters and Setters
	 * ###########################################################
	 */
}
