package com.theuberlab.jacronomicon.core;

import org.slf4j.LoggerFactory;

/**
 * The Acronym class is used for displaying matching acronyms in 
 * the GUI and for ensuring a proper format when adding acronyms
 * to a custom dataset.
 * It would probably be silly to instatiate all acronyms in memory
 * the entire time the application is running. This is intended to
 * be light weight.
 * 
 * @author Aaron Forster 
 * @date Feb 3, 2015
 */
public class Acronym {
	/*
	 * ###########################################################
	 *    Attributes
	 * ###########################################################
	 */
	private final static org.slf4j.Logger logger = LoggerFactory
			.getLogger(Acronym.class);
	/**
	 * The DB key for this acronym. Within the database itself
	 * acronymID is guaranteed unique. shortName however is not.
	 */
	protected int acronymID;
	/** 
	 * The acronym itself. I.E. "TLA," "SOP." or "HMF" for 
	 * "Three Letter Acronym,"
	 * "Standard Operating Procedure," or "Head Mother F***er"
	 * respectively.
	 */
	protected String shortName;
	/** 
	 * The expanded form of the acronym. "Three Letter Acronym,"
	 * "Standard Operating Procedure," or "Head Mother F***er" for
	 * "TLA," "SOP." or "HMF" respectively.
	 */
	protected String fullName;
	/**
	 * Extra notes about the acronym. This could include the definition 
	 * of the acronym (if it is not adequately self-describing,) 
	 * notes about the acronym, usage or more. 
	 */
	protected String description;
	/**
	 * The category to which this acronym belongs.
	 */
	protected String category;
	/**
	 * The data source which contains this definition.
	 */
	protected DataSource dataSource;

	/*
	 * ###########################################################
	 *    Constructors
	 * ###########################################################
	 */
	public Acronym(String shortName, String fullName, String description, String category) {
		this.shortName = shortName;
		this.fullName = fullName;
		this.description = description;
		this.category = category;
	}
	
	/*
	 * ###########################################################
	 *    Methods
	 * ###########################################################
	 */

	/*
	 * ###########################################################
	 *    Getters and Setters
	 * ###########################################################
	 */
	public String getShortName() {
		return shortName;
	}
}
