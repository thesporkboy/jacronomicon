/**
 * Core files for JAcronomicon. 
 * 
 * @author Aaron Forster 
 * @date Jan 29, 2015
 */
package com.theuberlab.jacronomicon.core;